import React from 'react'
import './SearchBar.css'

export default function SearchBar(props) {
  const { value, placeholder, onChange } = props
  return (
    <div className="search">
      <input value={value} placeholder={placeholder} onChange={onChange} />
    </div>
  )
}
