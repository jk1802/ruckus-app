import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import './Loader.css'

export default function CircularIndeterminate() {
  return (
    <div className="loader">
      <CircularProgress color="inherit" style={{ marginLeft: '50%' }} />
    </div>
  )
}
