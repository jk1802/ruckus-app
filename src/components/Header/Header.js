import React from 'react'
import { useLocation, useHistory } from 'react-router-dom'
import './Header.css'

export default function Header(props) {
  const currentLocation = useLocation().pathname
  const history = useHistory()
  return (
    <div className="header">
      <h1>{props.header}</h1>
      <div className="link">
        {currentLocation !== '/' ? (
          <button onClick={() => history.goBack()}>
            <span>Back</span>
          </button>
        ) : (
          ''
        )}
      </div>
      <ul>
        <li>{props.subHeader}</li>
        {props.address ? <p>Address: {props.address}</p> : ''}
        {props.rating ? (
          <li>
            Rating:{' '}
            <span style={{ color: '#' + props.ratingColor, fontWeight: 800 }}>
              {props.rating} {props.ratingText}
            </span>
          </li>
        ) : (
          ''
        )}
      </ul>
    </div>
  )
}
