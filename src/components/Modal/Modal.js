import React from 'react'
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import './Modal.css'

const useStyles = makeStyles({
  media: {
    height: 240,
  },
})

export default function Modal(props) {
  const classes = useStyles()

  return (
    <Card className="modal">
      <Link
        className="link"
        to={{
          pathname: props.path,
          state: {
            ...props,
          },
        }}
      >
        <CardActionArea>
          <CardMedia className={classes.media} image={props.image} />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {props.title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {props.description ? props.description : props.address}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {props.rating ? <div>Rating: {props.rating}</div> : ''}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Link>
    </Card>
  )
}
