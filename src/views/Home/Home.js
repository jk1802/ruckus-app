import React, { Component } from 'react'
import Modal from '../../components/Modal'
import Header from '../../components/Header'
import SearchBar from '../../components/SearchBar'
import Loader from '../../components/Loader'
import LightBoxContainer from '../../components/Lightbox'
import './Home.css'
import axios from 'axios'

const apiPrefix = process.env.REACT_APP_COLLECTIONS || 'http://localhost:3001'

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      isLoading: true,
      inputText: '',
    }
  }

  componentDidMount() {
    this.getRestaurants()
  }

  async getRestaurants() {
    const response = await axios.get(apiPrefix + '/api')
    let images = []
    for (let collection of response.data.collections) {
      for (let key in collection) {
        images.push(collection[key].image_url)
      }
    }
    this.setState({ data: response.data.collections, images, isLoading: false })
  }

  renderCards() {
    const { data, inputText } = this.state
    return data.map((restaurant) => {
      if (!inputText) {
        return (
          <Modal
            key={restaurant.collection.collection_id}
            image={restaurant.collection.image_url}
            title={restaurant.collection.title}
            description={restaurant.collection.description}
            collectionId={restaurant.collection.collection_id}
            path={'/restaurants'}
          />
        )
      } else if (
        inputText &&
        restaurant.collection.title.toLowerCase().includes(inputText.toLowerCase())
      ) {
        return (
          <Modal
            key={restaurant.collection.collection_id}
            image={restaurant.collection.image_url}
            title={restaurant.collection.title}
            description={restaurant.collection.description}
            collectionId={restaurant.collection.collection_id}
            path={'/restaurants'}
          />
        )
      }
    })
  }

  render() {
    const { isLoading, images, inputText } = this.state
    return (
      <>
        <Header
          header="An Unofficial Yelp Wannabe"
          subHeader="Browse by Cuisine"
        />
        <SearchBar
          value={inputText}
          placeholder={'Search category'}
          onChange={(e) => {
            this.setState({
              inputText: e.target.value,
            })
          }}
        />
        <LightBoxContainer images={images} />
        {isLoading ? (
          <Loader />
        ) : (
          <div className="container">{this.renderCards()}</div>
        )}
      </>
    )
  }
}
