import React, { Component } from 'react'
import Header from '../../components/Header'
import './Restaurant.css'
import axios from 'axios'

const apiPrefix = process.env.REACT_APP_COLLECTIONS || 'http://localhost:3001'

export default class Restaurant extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      id: null,
      address: null,
      cuisines: null,
      rating: null,
      ratingColor: null,
      ratingText: null,
      highlights: [],
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0)
    this.getRestaurantsDetails()
  }

  async getRestaurantsDetails() {
    const { id } = this.props.location.state
    const response = await axios.post(apiPrefix + '/restaurant', {
      id: id,
    })
    const data = response.data
    this.setState({
      address: data.location.address,
      cuisines: data.cuisines,
      rating: data.user_rating.aggregate_rating,
      ratingColor: data.user_rating.rating_color,
      ratingText: data.user_rating.rating_text,
      highlights: data.highlights,
    })
    return response
  }

  renderHighlights() {
    const { highlights } = this.state
    return highlights.map((highlight, index) => {
      return (
        <li key={index} className="highlight">
          {highlight}
        </li>
      )
    })
  }

  render() {
    const { title, image } = this.props.location.state
    const { address, cuisines, rating, ratingColor, ratingText } = this.state
    return (
      <>
        <Header
          header={title}
          subHeader={cuisines}
          address={address}
          rating={rating}
          ratingColor={ratingColor}
          ratingText={ratingText}
        />{' '}
        <div className="container">
          <img className="featured-img" src={image} />
        </div>
        <h2 className="highlights-label">Restaurant Highlights</h2>
        <ul className="highlights">{this.renderHighlights()}</ul>
      </>
    )
  }
}
