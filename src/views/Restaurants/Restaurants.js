import React, { Component } from 'react'
import Header from '../../components/Header'
import Modal from '../../components/Modal'
import Loader from '../../components/Loader'
import './Restaurants.css'
import axios from 'axios'

const apiPrefix = process.env.REACT_APP_COLLECTIONS || 'http://localhost:3001'

export default class Restaurants extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      collectionId: null,
      isLoading: true,
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0)
    this.getRestaurantsDetails()
  }

  async getRestaurantsDetails() {
    const { collectionId } = this.props.location.state
    const response = await axios.post(apiPrefix + '/collections', {
      collectionId: collectionId,
    })
    this.setState({ data: response.data.restaurants, isLoading: false })
    return response
  }

  renderCards() {
    const { data } = this.state
    return data.map((restaurant) => {
      const {
        id,
        featured_image,
        name,
        location,
        user_rating,
      } = restaurant.restaurant
      return (
        <Modal
          key={id}
          id={id}
          image={featured_image}
          title={name}
          address={location.address}
          rating={user_rating.aggregate_rating}
          path={'/restaurant'}
        />
      )
    })
  }

  render() {
    const { isLoading } = this.state
    const { title, description } = this.props.location.state
    return (
      <>
        <Header header={title} subHeader={description} />
        {isLoading ? (
          <Loader />
        ) : (
          <div className="container">{this.renderCards()}</div>
        )}
      </>
    )
  }
}
