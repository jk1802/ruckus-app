import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Home from './views/Home'
import Restaurant from './views/Restaurant'
import Restaurants from './views/Restaurants'
import './App.css'

const BaseLayout = () => (
  <Switch>
    <Route path="/" exact component={Home} />
    <Route path="/restaurants" exact component={Restaurants} />
    <Route path="/restaurant" exact component={Restaurant} />
  </Switch>
)

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <BaseLayout />
      </BrowserRouter>
    )
  }
}
